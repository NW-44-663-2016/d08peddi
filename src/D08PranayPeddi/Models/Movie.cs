﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace D08PranayPeddi.Models
{
    public class Movie
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public int ReleaseYear { get; set; }
        public int Runtime { get; set; }

    }
}
