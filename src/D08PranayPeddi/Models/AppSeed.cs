﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity;

namespace D08PranayPeddi.Models
{
    public class AppSeed
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (context==null) { return; }
            context.Database.Migrate();

            if (!context.Locations.Any()) {
                var l1 = context.Locations.Add(new Location() { Latitude = 17.4126272, Longitude = 78.268675, Place = "Missouri", Country = "India" }).Entity;
                var l2 = context.Locations.Add(new Location() { Latitude = 36.47558772, Longitude = 78.8797, Place = "Nebraska", Country = "India" }).Entity;
                var l3 = context.Locations.Add(new Location() { Latitude = 34.4126272, Longitude = 78.675858, Place = "Ohio", Country = "India" }).Entity;
                var l4 = context.Locations.Add(new Location() { Latitude = 67.4789576, Longitude = 78.746453, Place = "Hyd", Country = "India" }).Entity;
                var l5 = context.Locations.Add(new Location() { Latitude = 89.67894900, Longitude = 78.267686, Place = "Hyderabad", Country = "India" }).Entity;
                var l6 = context.Locations.Add(new Location() { Latitude = 90.4756782, Longitude = 78.978575, Place = "Hyd", Country = "India" }).Entity;
                var l7 = context.Locations.Add(new Location() { Latitude = 56.890786562, Longitude = 78.256575, Place = "Hyderabad", Country = "India" }).Entity;

                context.Directors.AddRange(
       new Director() { DirectorName = "Preethi", Email = "pp@gmail.com", Phone = "9876789", Experience = "78", Location = l1 },
       new Director() { DirectorName = "Soumya Vishal", Email = "pp@gmail.com", Phone = "9876789", Experience = "78", Location = l2 },
       new Director() { DirectorName = "Anvesh Kolluri", Email = "pp@gmail.com", Phone = "9876789", Experience = "78", Location = l3 },
       new Director() { DirectorName = "Preethi Gangireddy", Email = "pp@gmail.com", Phone = "9876789", Experience = "78", Location = l4 },
       new Director() { DirectorName = "Praneeth Kolluri", Email = "pp@gmail.com", Phone = "9876789", Experience = "78", Location = l5 },
       new Director() { DirectorName = "Swathi Peddi", Email = "pp@gmail.com", Phone = "9876789", Experience = "78", Location = l6 },
       new Director() { DirectorName = "Prabhakar Ganji", Email = "pp@gmail.com", Phone = "9876789", Experience = "78", Location = l7 });
                

               
                
            }

            context.SaveChanges();
        }
    }
}
