﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace D08PranayPeddi.Models
{
    public class Director
    {
        [ScaffoldColumn(false)]
        [Key]
        public int DirectorID { get; set; }

        [Display(Name = "Director's Full Name")]
        public string DirectorName { get; set; }
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "State the years of Experience")]

        public string Experience { get; set; }

        [Display(Name = "Education Qualifications")]
        public string EducationQualification { get; set; }

        [ScaffoldColumn(false)]
        public int LocationID { get; set; }


        // Navigation Property
        public virtual Location Location { get; set; }
    }
}
