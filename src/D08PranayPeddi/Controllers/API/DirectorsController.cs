using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08PranayPeddi.Models;

namespace D08PranayPeddi.Controllers
{
    [Produces("application/json")]
    [Route("api/Directors")]
    public class DirectorsController : Controller
    {
        private ApplicationDbContext _context;

        public DirectorsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Directors
        [HttpGet]
        public IEnumerable<Director> GetDirectors()
        {
            return _context.Directors;
        }

        // GET: api/Directors/5
        [HttpGet("{id}", Name = "GetDirector")]
        public IActionResult GetDirector([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Director director = _context.Directors.Single(m => m.DirectorID == id);

            if (director == null)
            {
                return HttpNotFound();
            }

            return Ok(director);
        }

        // PUT: api/Directors/5
        [HttpPut("{id}")]
        public IActionResult PutDirector(int id, [FromBody] Director director)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != director.DirectorID)
            {
                return HttpBadRequest();
            }

            _context.Entry(director).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DirectorExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Directors
        [HttpPost]
        public IActionResult PostDirector([FromBody] Director director)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Directors.Add(director);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DirectorExists(director.DirectorID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetDirector", new { id = director.DirectorID }, director);
        }

        // DELETE: api/Directors/5
        [HttpDelete("{id}")]
        public IActionResult DeleteDirector(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Director director = _context.Directors.Single(m => m.DirectorID == id);
            if (director == null)
            {
                return HttpNotFound();
            }

            _context.Directors.Remove(director);
            _context.SaveChanges();

            return Ok(director);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DirectorExists(int id)
        {
            return _context.Directors.Count(e => e.DirectorID == id) > 0;
        }
    }
}