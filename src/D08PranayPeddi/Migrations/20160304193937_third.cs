using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace D08PranayPeddi.Migrations
{
    public partial class third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Director_Location_LocationID", table: "Director");
            migrationBuilder.AddForeignKey(
                name: "FK_Director_Location_LocationID",
                table: "Director",
                column: "LocationID",
                principalTable: "Location",
                principalColumn: "LocationID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_Director_Location_LocationID", table: "Director");
            migrationBuilder.AddForeignKey(
                name: "FK_Director_Location_LocationID",
                table: "Director",
                column: "LocationID",
                principalTable: "Location",
                principalColumn: "LocationID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
