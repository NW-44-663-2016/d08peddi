using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using D08PranayPeddi.Models;

namespace D08PranayPeddi.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160304193937_third")]
    partial class third
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("D08PranayPeddi.Models.Director", b =>
                {
                    b.Property<int>("DirectorID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DirectorName");

                    b.Property<string>("EducationQualification");

                    b.Property<string>("Email");

                    b.Property<string>("Experience");

                    b.Property<int>("LocationID");

                    b.Property<string>("Phone");

                    b.HasKey("DirectorID");
                });

            modelBuilder.Entity("D08PranayPeddi.Models.Location", b =>
                {
                    b.Property<int>("LocationID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("D08PranayPeddi.Models.Movie", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("ReleaseYear");

                    b.Property<int>("Runtime");

                    b.Property<string>("Title");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("D08PranayPeddi.Models.Director", b =>
                {
                    b.HasOne("D08PranayPeddi.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
